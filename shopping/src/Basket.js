import React, { useState } from 'react'

const Basket = ({ products, setAddToCart}) => {
  // [ ] braces or "curly brackets" { }

  

 

  return (

    <div className='basket'>
      <h3>Product</h3>
      <div className='products'>

        { products.map(v=> 
           (
            <div className='productitems'>
              <center><img src={ v.image} style={{ width:'100px'}}/></center>
              <div>
                <p>{ v.name}</p>
                
              </div>
              <div>
                <p>{ v.price}</p>
              </div>
              <div>
                <button onClick={()=>setAddToCart(v)}> Add to cart </button>
              </div>
            </div>  
          )
        )}
        
      
      </div>
    </div>
  )
}

export default Basket