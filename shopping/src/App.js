import './App.css';
import Header from './Header';
import Basket from "./Basket";
import Cart from "./Cart";
import React, { useState, useEffect } from 'react';
const { faker } = require('@faker-js/faker');

// [ ] braces or "curly brackets" { }

function App() {

  const  [ products, setProducts ] = useState([]); 
  let  [ total, setTotal ] = useState(0); 
 

  useEffect(()=>{ 
    const items = [...Array(4) ].map(()=>{ 
      return { 
        id:faker.datatype.uuid(),
        name:faker.commerce.productName(),
        price:faker.commerce.price(),
        image:faker.image.avatar()
      }
    });
    setProducts(items);
  },[])

  useEffect(()=>{ 
    setTotal(total);
    console.log('total',total);
  },[total])

  
  
  function addToCart(item){ 
    total+= +item.price;
    setTotal(total);
  }

  
  
  return (
    
    <div className="App">
      <Header />
      <div className='containerOfChild'>
        <div>
          <Basket products= { products} setAddToCart={ cart=> addToCart(cart) } />
        </div>
        <div>
          <Cart total={ total } />
        </div>
      </div>
    </div>
    );
}

export default App;
// faker.image.avatar()

// id: faker.datatype.uuid(),
//     name: faker.commerce.productName(),
//     price: faker.commerce.price(),
//     image: faker.random.image(),