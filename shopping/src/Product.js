import React from 'react'

export default function Product(props) {
    const{product} = props;
  return (
    <div>
        <img className='small' src={Product.image} alt={Product.name}></img>
        <h3>{product.name }</h3>
        <div>${product.price}</div>
        <div>
            <button>Add to cart</button>
        </div>
    </div>
  )
}
